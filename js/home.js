//home
var acc = document.getElementsByClassName("important-ques__item-title");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
}
var acc = document.getElementsByClassName("menu-mobile-select-lang__link");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
}
var acc = document.getElementsByClassName("menu-mobile__link-view");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
}
// animation link #id
$(document).ready(function(){
    $( "a.main-header-banner__link" ).click(function( event ) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
    });
});
//home
AOS.init();
//
$('.menu-select-lang').click( function () {
    $(this).parent().toggleClass("active");
    $('.menu-select-lang__overlay').addClass('active');
});
$('.menu-select-lang__overlay').click( function () {
    $(this).removeClass('active');
    $(this).parent().removeClass('active');
})
$('.menu-select-lang__eng').click( function () {
    $('.menu-select-lang__text').text('EN');
    $('.menu-item').removeClass('active');
    $('.menu-select-lang__overlay').removeClass('active');
})
$('.menu-select-lang__vit').click( function () {
    $('.menu-select-lang__text').text('VI');
    $('.menu-item').removeClass('active');
    $('.menu-select-lang__overlay').removeClass('active');
});