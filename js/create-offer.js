//select buy/sell
$(function() {                       
    $('.create-offer__action-button').click( function () {
        $('.create-offer__action-button.active').removeClass("active");
        $(this).addClass("active"); 
    })
});
$(function() {                       
    $('.create-offer__option-button').click( function () {
        $('.create-offer__option-button.active').removeClass("active");
        $(this).addClass("active"); 
    })
});
$('.create-offer__item-edit').click( function () {
    $(this).prev().children().next().children().css('display','none');
    $(this).prev().children().next().children().next().css('display','block');
    $(this).prev().children().next().children().next().val($(this).prev().children().next().children(":first").text().replace(/ /g,''));
})
$('#buy').click( function () {
    $('.create-offer__item-content--text-action').text("trả");
    $('.create-offer__item-content--text-buy-sell').text("bán");
    $('.create-offer__item-content--text-min-max').text("tối thiểu");
})
$('#sell').click( function () {
    $('.create-offer__item-content--text-action').text("nhận");
    $('.create-offer__item-content--text-buy-sell').text("mua");
    $('.create-offer__item-content--text-min-max').text("tối đa");
})

// select pay
$(document).ready(function(){ 
    $('.create-offer__item-content--select').change(function(){
        if($('.create-offer__item-content--select').val() == 2) {
            $('.transfer-name').css('display', 'block');                                                 
        }else {
            $('.transfer-name').css('display', 'none');  
        }
    });
    $('#select-option').change(function(){
        if(($('#select-option').val() == 1) || ($('#select-option').val() == 'default')) {
           $('.option-title').text('BTC');                                           
        } 
        else if($('#select-option').val() == 2) {
            $('.option-title').text('LTC');    
        }
        else if($('#select-option').val() == 3) {
            $('.option-title').text('USDT');    
        }
    });
});
// show price

var selectAcion = false;
var selectOption = false;
$(document).ready(function(){
    showPrice = () => {
    if((selectAcion === true) && (selectOption === true)) {
        $(".create-offer__item-content--value-price").show();
    }else {
        $(".create-offer__item-content--value-price").hide();
    }
}
});
handleSelectAction = () => {
    selectAcion = true;
    showPrice();
}
handleSelectOptionn = () => {
    selectOption = true;
    showPrice();
}