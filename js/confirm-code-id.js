
var btnUpdate = document.querySelector('.btn-update-confirm');
btnUpdate.onclick = function () {
    var dataNameWrite = document.querySelector('input[name="name-write"]');
    var dataCodeWrite = document.querySelector('input[name="code-write"]');
    var dataName = document.querySelector('input[name="name"]');
    var dataCode = document.querySelector('input[name="code"]');
    dataName.value = dataNameWrite.value;
    dataCode.value = dataCodeWrite.value;
}