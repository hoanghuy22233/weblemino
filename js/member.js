
$('#up-member-silver').click( function () {
    $('.member-update-text').text('Silver');
    $('.member-update-price').text('2.000.000VND');
});
$('#up-member-gold').click( function () {
    $('.member-update-text').text('Gold');
    $('.member-update-price').text('4.000.000VND');
});
$('#up-member-diamond').click( function () {
    $('.member-update-text').text('Diamond');
    $('.member-update-price').text('15.000.000VND');
})
$('.create-offer__item-content--select').change(function(){
    if($('.create-offer__item-content--select').val() == 'wallet') {
        $('.payment-wallet').css('display', 'block');  
        $('.payment-bank').css('display', 'none');                                                
    } else if($('.create-offer__item-content--select').val() == 'bank') {
        $('.payment-bank').css('display', 'block');    
        $('.payment-wallet').css('display', 'none');                                                
    } else if($('.create-offer__item-content--select').val() == 'momo') {
        $('.payment-bank').css('display', 'none');  
    }
});
$('#btn-payment-bank-otp').click( function () {
    $('#otp').css('display', 'block');
    $(this).css('display', 'none');
    $('#btn-payment-bank-confirm').css('display', 'block');
    $('#confirm-phone__get-code-mail').click();
})
function startTimer(duration, display, btn) {
    var timer = duration, minutes, seconds;
    const timeValue = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            btn.disabled = false;
            clearInterval(timeValue);
        }
    }, 1000);
}
var btnGetCodeMail = document.querySelector('#confirm-phone__get-code-mail');
btnGetCodeMail.onclick = function () {
    var minutes = 60 * 2;
    display = document.querySelector('.time-mail');
    var btn = this;
    btn.disabled = true;
    startTimer(minutes, display, btn);
};