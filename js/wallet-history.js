$(document).ready(function () { // đợi web load xong
    var sefl = this;

    sefl.init = function () {
        var pathname = location.search;
        currency = pathname.replace('?currency=', ''); 
        if (currency) sefl.showAction(currency);
    }

    sefl.showAction = function (currency) {
        switch (currency) {
            case 'vnd':
                $('.history-selector').text('VND');
                break;
            case 'btc':
                $('.history-selector').text('BTC');
                break;
            case 'ltc':
                $('.history-selector').text('LTC');
                break;
            case 'usdt':
                $('.history-selector').text('USDT');
                break;
        }
    }

    // Withdraw
    $('#vnd').click(function () {
        sefl.showAction('vnd');
    });

    $('#btc').click(function () {
        sefl.showAction('btc');
    });

    $('#ltc').click(function () {
        sefl.showAction('ltc');
    });

    $('#usdt').click(function () {
        sefl.showAction('usdt');
    });

    $('.withdraw-type-wallet__item').click(function () {
        $('.withdraw-type-wallet__item.active').removeClass('active');
        $(this).addClass('active');
    });

    sefl.init();
});

function selectHistory(action) {
    var i;
    var x = document.getElementsByClassName("wallet-history__content-item");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(action).style.display = "block";
}