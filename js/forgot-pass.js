function startTimer(duration, display, btn) {
    var timer = duration, minutes, seconds;
    const timeValue = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            btn.disabled = false;
            clearInterval(timeValue);
        }
    }, 1000);
}
var btnGetCodeMail = document.querySelector('#confirm-phone__get-code-mail');
btnGetCodeMail.onclick = function () {
    var minutes = 60 * 2;
    display = document.querySelector('.time-mail');
    var btn = this;
    btn.disabled = true;
    startTimer(minutes, display, btn);
};