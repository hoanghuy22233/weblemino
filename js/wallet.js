// Wallet, wallet-history, confirm-wallet


$('.wallet-item__action-button--swap').click( function () {
    var currency = $(this).val();
    window.location = `swap-money.html?currency=${currency}`;
});

$('.wallet-item__action-button--withdrawal').click( function () {
    var currency = $(this).val();
    window.location = `withdraw.html?currency=${currency}`;
});

$('.wallet-item__action-button--deposit').click( function () {
    var currency = $(this).val();
    window.location = `deposit.html?currency=${currency}`;
});

$('.wallet-item__action-button--more').click( function () {
    var currency = $(this).val();
    window.location = `wallet-history.html?currency=${currency}`;
});

// wallet-history