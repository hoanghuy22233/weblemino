$(document).ready(function () { // đợi web load xong
    var sefl = this;

    sefl.init = function () {
        var pathname = location.search;
        currency = pathname.replace('?currency=', ''); 
        if (currency) sefl.showAction(currency);
    }

    sefl.showAction = function (currency) {
        switch (currency) {
            case 'vnd':
                $('.withdraw-vnd').addClass('active');
                $('.withdraw-type-wallet__wapper').removeClass('active');
                $('.withdraw-coin').removeClass('active');
                break;
            case 'btc':
                $('.withdraw-coin').addClass('active');
                $('.deposit-selector').text('BTC');
                $('.withdraw-type-wallet__wapper').removeClass('active');
                $('.withdraw-vnd').removeClass('active');
                break;
            case 'ltc':
                $('.withdraw-coin').addClass('active');
                $('.deposit-selector').text('LTC');
                $('.withdraw-type-wallet__wapper').removeClass('active');
                $('.withdraw-vnd').removeClass('active');
                break;
            case 'usdt':
                $('.withdraw-coin').addClass('active');
                $('.deposit-selector').text('USDT');
                $('.withdraw-vnd').removeClass('active');
                $('.withdraw-type-wallet__wapper').addClass('active');
                break;
        }
    }

    // Withdraw
    $('#vnd').click(function () {
        sefl.showAction('vnd');
    });

    $('#btc').click(function () {
        sefl.showAction('btc');
    });

    $('#ltc').click(function () {
        sefl.showAction('ltc');
    });

    $('#usdt').click(function () {
        sefl.showAction('usdt');
    });

    $('.withdraw-type-wallet__item').click(function () {
        $('.withdraw-type-wallet__item.active').removeClass('active');
        $(this).addClass('active');
    });

    sefl.init();
});

$('.create-offer__item-content--select').change(function(){
    if($('.create-offer__item-content--select').val() == 'wallet') {
        $('.payment-wallet').css('display', 'block');  
        $('.payment-bank').css('display', 'none');                                                
    } else if($('.create-offer__item-content--select').val() == 'bank') {
        $('.payment-bank-deposit').css('display', 'block');    
        $('.payment-wallet').css('display', 'none');                                                
    } else if($('.create-offer__item-content--select').val() == 'momo') {
        $('.payment-bank-deposit').css('display', 'none');  
    }
});
$('#btn-payment-bank-otp').click( function () {
    $('#otp').css('display', 'block');
    $(this).css('display', 'none');
    $('#btn-payment-bank-confirm').css('display', 'block');
    $('#confirm-phone__get-code-mail').click();
})
function startTimer(duration, display, btn) {
    var timer = duration, minutes, seconds;
    const timeValue = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            btn.disabled = false;
            clearInterval(timeValue);
        }
    }, 1000);
}
var btnGetCodeMail = document.querySelector('#confirm-phone__get-code-mail');
btnGetCodeMail.onclick = function () {
    var minutes = 60 * 2;
    display = document.querySelector('.time-mail');
    var btn = this;
    btn.disabled = true;
    startTimer(minutes, display, btn);
};
// $('#vnd').click(function () {
//     $('.withdraw-vnd').addClass('active');
//     $('.withdraw-type-wallet__wapper').removeClass('active');
//     $('.withdraw-coin').removeClass('active');
// });
// $('#btc').click(function () {
//     $('.withdraw-coin').addClass('active');
//     $('.deposit-selector').text('BTC');
//     $('.withdraw-type-wallet__wapper').removeClass('active');
//     $('.withdraw-vnd').removeClass('active');
// });
// $('#ltc').click(function () {
//     $('.withdraw-coin').addClass('active');
//     $('.deposit-selector').text('LTC');
//     $('.withdraw-type-wallet__wapper').removeClass('active');
//     $('.withdraw-vnd').removeClass('active');
// });
// $('#usdt').click(function () {
//     $('.withdraw-coin').addClass('active');
//     $('.deposit-selector').text('USDT');
//     $('.withdraw-vnd').removeClass('active');
//     $('.withdraw-type-wallet__wapper').addClass('active');
// });

// $('.withdraw-type-wallet__item').click(function () {
//     $('.withdraw-type-wallet__item.active').removeClass('active');
//     $(this).addClass('active');
// })
