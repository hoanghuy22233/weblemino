changeBlurb = () => {
    var changeBlurbArea = document.querySelector('.confirm-blurb__textarea');
    var changeBlurb = document.querySelector('.account-content__text-value--blurb');
    changeBlurb.textContent = changeBlurbArea.value;
}
//Timeout get code
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    const timeValue = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            clearInterval(timeValue);
        }
    }, 1000);
}
//Confirm Mail
var btnGetCodeMail = document.querySelector('#confirm-phone__get-code-mail');
btnGetCodeMail.onclick = function () {
    var minutes = 60 * 2;
    display = document.querySelector('.time-mail');
    this.disabled = true;
    btnConfirmMail.disabled = false;
    setTimeout( function () {
        this.disabled = false;
    }, minutes);
    startTimer(minutes, display);
};

var btnConfirmMail = document.querySelector('.confirm-email__button');
btnConfirmMail.disabled = true;
var inputConfirmMail = document.querySelector('input[name="code-mail"]');
inputConfirmMail.onchange = function () {
    btnConfirmMail.disabled = false;
}
//Confirm Phone
var btnGetCodePhone = document.querySelector('#confirm-phone__get-code-phone');
btnGetCodePhone.onclick = function () {
    var minutes = 60 * 2;
    display = document.querySelector('.time-phone');
    this.disabled = true;
    setTimeout( function () {
        this.disabled = false;
    }, minutes);
    startTimer(minutes, display);
};

var btnConfirmPhone = document.querySelector('.confirm-phone__button');
btnConfirmPhone.disabled = true;
var inputConfirmPhone = document.querySelector('input[name="code-phone"]');
inputConfirmPhone.onchange = function () {
    btnConfirmPhone.disabled = false;
}
$('.account-content__update').click( function () {
    window.location = "member.html";
})
$('.account-content__confirm').click( function () {
    window.location = "confirm-id.html";
})