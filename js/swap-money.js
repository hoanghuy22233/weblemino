

$(document).ready(function () { // đợi web load xong
    var sefl = this;

    sefl.init = function () {
        var pathname = location.search;
        currency = pathname.replace('?currency=', ''); 
        if (currency) sefl.showAction(currency);
    }

    sefl.showAction = function (currency) {
        switch (currency) {
            case 'vnd':
                
                break;
            case 'btc':
                $('.swap-money-title').text('BTC');
                $('#btc-select').css('display','none');
                $('#ltc-select').css('display','flex');
                $('#usdt-select').css('display','flex');
                break;
            case 'ltc':
                $('.swap-money-title').text('LTC');
                $('#ltc-select').css('display','none');
                $('#btc-select').css('display','flex');
                $('#usdt-select').css('display','flex');
                break;
            case 'usdt':
                $('.swap-money-title').text('USDT');
                $('#usdt-select').css('display','none');
                $('#ltc-select').css('display','flex');
                $('#btc-select').css('display','flex');
                break;
        }
    }

    // Withdraw
    $('#vnd').click(function () {
        sefl.showAction('vnd');
    });

    $('#btc').click(function () {
        sefl.showAction('btc');
    });

    $('#ltc').click(function () {
        sefl.showAction('ltc');
    });

    $('#usdt').click(function () {
        sefl.showAction('usdt');
    });

    $('.withdraw-type-wallet__item').click(function () {
        $('.withdraw-type-wallet__item.active').removeClass('active');
        $(this).addClass('active');
    });

    sefl.init();
});
// Swap money 
$('.swap-money-item').click( function () {
    $('.swap-money-item.active').removeClass('active');
    $(this).addClass('active');
})
$('#btc-select').click( function () {
    $('.swap__title-selector').text('BTC');
})
$('#ltc-select').click( function () {
    $('.swap__title-selector').text('LTC');
})
$('#usdt-select').click( function () {
    $('.swap__title-selector').text('USDT');
})
// $('#btc').click( function () {
//     $('.swap-money-title').text('BTC');
//     $('#btc-select').css('display','none');
//     $('#ltc').css('display','flex');
//     $('#usdt-select').css('display','flex');
// })
// $('#ltc').click( function () {
//     $('.swap-money-title').text('LTC');
//     $('#ltc-select').css('display','none');
//     $('#btc-select').css('display','flex');
//     $('#usdt-select').css('display','flex');
// })
// $('#usdt').click( function () {
//     $('.swap-money-title').text('USDT');
//     $('#usdt-select').css('display','none');
//     $('#ltc-select').css('display','flex');
//     $('#btc-select').css('display','flex');
// })